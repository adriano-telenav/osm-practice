import metric.counter.HighWayCounter;
import metric.counter.PointOfInterestCounter;
import metric.counter.RecentWayCounter;
import measure.dataframe.HighWayDistanceMeasurer;
import measure.dataframe.NamedRoadDistanceMeasurer;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;


public class OsmDataframeStatisticsJob {

    public static void main(String[] args) throws Exception {
        final ArgumentsConfig config = new ArgumentsConfig(args);
        final SparkConf conf = new SparkConf().setAppName("osm-practice").setMaster(config.getMaster())
                .set("spark.sql.parquet.binaryAsString", "true");
        final SparkSession spark = new SparkSession.Builder().config(conf).getOrCreate();

        Dataset<Row> nodes = spark.read().parquet(config.getNodesPath());
        Dataset<Row> ways = spark.read().parquet(config.getWaysPath());

        final long numberOfPlacesOfInterest = PointOfInterestCounter.count(nodes);
        final long waysAddedLastWeek = RecentWayCounter.count(ways);
        final long highwayNumber = HighWayCounter.count(ways);

        final double untypedHighwayLength = new HighWayDistanceMeasurer().measure(nodes, ways);
        final NamedRoadDistanceMeasurer namedRoadDistanceMeasurer = new NamedRoadDistanceMeasurer();
        final double untypedTotalNamedRoadLength = namedRoadDistanceMeasurer.measure(nodes, ways);

        System.out.println("Number of palces of interest: " + numberOfPlacesOfInterest);
        System.out.println("Ways added last week: " + waysAddedLastWeek);
        System.out.println("Highways: " + highwayNumber);
        System.out.println("Total highway length: " + untypedHighwayLength);
        System.out.println("Total named road length: " + untypedTotalNamedRoadLength);
    }
}
