package metric.flatten;

import field.FieldNames;
import field.RelationMemberNames;
import field.TagFieldNames;
import field.WayNodeFieldNames;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.HashSet;
import java.util.Set;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;


public class Flattener {

    private static final String TAG = "tag";
    private static final String NODE = "node";
    private static final String RELATION = "relation";

    public static Dataset<Row> flattenTags(Dataset<Row> entities) {
        final FlattenParameters flattenParameters = new FlattenParameters.Builder()
                .withCollectionName(FieldNames.TAGS.getValue()).withElementName(TAG)
                .withElementField(TagFieldNames.KEY.getValue())
                .withElementField(TagFieldNames.VALUE.getValue()).build();
        return flatten(entities, flattenParameters);
    }

    public static Dataset<Row> flattenNodes(Dataset<Row> entities) {
        final FlattenParameters flattenParameters = new FlattenParameters.Builder()
                .withCollectionName(FieldNames.Way.NODES.getValue()).withElementName(NODE)
                .withElementField(WayNodeFieldNames.INDEX.getValue())
                .withElementField(WayNodeFieldNames.NODEID.getValue()).build();
        return flatten(entities, flattenParameters);
    }

    public static Dataset<Row> flattenRelations(Dataset<Row> entities) {
        final FlattenParameters flattenParameters = new FlattenParameters.Builder()
                .withCollectionName(FieldNames.Relation.MEMBERS.getValue()).withElementName(RELATION)
                .withElementField(RelationMemberNames.ID.getValue())
                .withElementField(RelationMemberNames.ROLE.getValue())
                .withElementField(RelationMemberNames.TYPE.getValue())
                .build();
        return flatten(entities, flattenParameters);
    }

    private static class FlattenParameters {

        private final String collectionName;
        private final String elementName;
        private final Set<String> elementFields;

        FlattenParameters(Builder builder) {
            collectionName = builder.collectionName;
            elementName = builder.elementName;
            elementFields = builder.elementFields;
        }

        public static final class Builder {
            private String collectionName;
            private String elementName;
            private Set<String> elementFields = new HashSet<>();

            private Builder() {
            }

            Builder withCollectionName(String collectionName) {
                this.collectionName = collectionName;
                return this;
            }

            Builder withElementName(String elementName) {
                this.elementName = elementName;
                return this;
            }

            Builder withElementField(String elementField) {
                elementFields.add(elementField);
                return this;
            }

            public FlattenParameters build() {
                return new FlattenParameters(this);
            }
        }
    }

    private static Dataset<Row> flatten(Dataset<Row> entities, FlattenParameters parameters) {
        Dataset<Row> exploded = entities.withColumn(parameters.elementName, explode(col(parameters.collectionName)));
        final Set<String> elementFields = parameters.elementFields;
        for (String elementField : elementFields) {
            exploded = exploded.withColumn(elementField,
                    col(parameters.elementName + "." + elementField)).drop(parameters.collectionName);
        }
        return exploded.drop(parameters.elementName);
    }
}
