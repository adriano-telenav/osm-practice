package metric.counter;

import field.TagFieldNames;
import metric.flatten.Flattener;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static field.FieldNames.ID;
import static org.apache.spark.sql.functions.col;


public final class PointOfInterestCounter {

    private PointOfInterestCounter() {}

    public static final String AMENITY = "amenity";
    public static final String SHOP = "shop";
    public static final String HISTORIC = "historic";

    public static long count(final Dataset<Row> nodes) {
        final Dataset<Row> flattenedNodes = Flattener.flattenTags(nodes);
        final Dataset<Row> pois = flattenedNodes.filter(col(TagFieldNames.KEY.getValue()).isin(AMENITY, SHOP, HISTORIC));
        return pois.groupBy(ID.getValue()).count().count();
    }
}
