package metric.counter;

import field.FieldNames;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static org.apache.spark.sql.functions.col;


public final class RecentWayCounter {

    private RecentWayCounter() {}

    private static final int MILLISECONDS_IN_A_WEEK = 1000 * 60 * 60 * 24 * 7;

    public static long count(final Dataset<Row> ways) {
        long now = System.currentTimeMillis();
        final long oneWeekAgo = now - MILLISECONDS_IN_A_WEEK;
        return ways.filter(col(FieldNames.TIMESTAMP.getValue()).gt(oneWeekAgo))
                .groupBy(FieldNames.ID.getValue()).count().count();
    }
}