package metric.counter;

import function.filter.UntypedHighwayFilter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;


public final class HighWayCounter {

    private HighWayCounter() {}

    public static long count(final Dataset<Row> ways) {
        return ways.filter(new UntypedHighwayFilter()).count();
    }
}
