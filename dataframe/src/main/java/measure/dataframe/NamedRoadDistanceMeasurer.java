package measure.dataframe;

import field.FieldNames;
import field.RelationMemberNames;
import function.filter.UntypedNamedRoadFilter;
import function.filter.UntypedNamedRoadRelationFilter;
import function.filter.WayInRelationFilter;
import metric.flatten.Flattener;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;


public class NamedRoadDistanceMeasurer extends DataframeWayDistanceMeasurer{
    private static final String RELATION_ID = "relation_id";

    public NamedRoadDistanceMeasurer() {
        super(new UntypedNamedRoadFilter());
    }

    public double measureRoadsInRelations(final Dataset<Row> nodes, final Dataset<Row> ways,
            final Dataset<Row> relations) {
        final Dataset<Row> filtered = relations.filter(new UntypedNamedRoadRelationFilter())
                .withColumnRenamed(FieldNames.ID.getValue(), RELATION_ID);
        final Dataset<Row> flattened = Flattener.flattenRelations(filtered);
        final Dataset<Row> flatWaysInRelations = flattened.filter(new WayInRelationFilter())
                .drop(RelationMemberNames.ROLE.getValue())
                .drop(RelationMemberNames.TYPE.getValue())
                .withColumnRenamed(FieldNames.ID.getValue(), WAY_ID);

        final Dataset<Row> joined = flatWaysInRelations
                .join(ways, flatWaysInRelations.col(WAY_ID)
                        .equalTo(ways.col(FieldNames.ID.getValue())), "outer")
                .drop(FieldNames.ID.getValue());

        final Dataset<Row> wayDistances = measureWays(nodes, joined);
        return sumWaysDistances(wayDistances);
    }
}
