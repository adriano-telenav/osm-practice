package measure.dataframe;

import field.FieldNames;
import field.WayNodeFieldNames;
import function.UntypedWayDistance;
import metric.flatten.Flattener;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.UserDefinedFunction;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.DoubleType;


public class DataframeWayDistanceMeasurer {

    private static final String TOTAL = "total";
    protected static final String WAY_ID = "wayId";
    private static final String NODE = "node";
    private static final String WAY_LENGTH = "way_length";

    private final FilterFunction<Row> dataframeFilter;

    public DataframeWayDistanceMeasurer(final FilterFunction<Row> dataframeFilter) {
        this.dataframeFilter = dataframeFilter;
    }

    public double measure(final Dataset<Row> nodes, final Dataset<Row> ways) {
        final Dataset<Row> filtered = ways.filter(dataframeFilter);

        final Dataset<Row> newWays = filtered.withColumnRenamed(FieldNames.ID.getValue(), WAY_ID);
        final Dataset<Row> wayDistances = measureWays(nodes, newWays);
        return sumWaysDistances(wayDistances);
    }

    protected Double sumWaysDistances(final Dataset<Row> wayDistances) {
        return wayDistances.agg(sum(WAY_LENGTH).alias(TOTAL)).first().getAs(TOTAL);
    }

    protected Dataset<Row> measureWays(final Dataset<Row> nodes, final Dataset<Row> newWays) {
        final Dataset<Row> nested = makeNodesWithCoordinates(nodes, newWays);
        return measureEachWay(nested);
    }

    private Dataset<Row> measureEachWay(final Dataset<Row> nested) {
        final UserDefinedFunction highwayLength = udf(new UntypedWayDistance(), DoubleType);
        return nested.select(highwayLength.apply(col(FieldNames.Way.NODES.getValue())).as(WAY_LENGTH));
    }

    private Dataset<Row> makeNodesWithCoordinates(final Dataset<Row> nodes, final Dataset<Row> newWays) {
        final Dataset<Row> waysFlattenedNodes = Flattener.flattenNodes(newWays);
        final Dataset<Row> joined = waysFlattenedNodes.join(nodes, nodes.col(FieldNames.ID.getValue())
                .equalTo(waysFlattenedNodes.col(WayNodeFieldNames.NODEID.getValue()))).drop(FieldNames.ID.getValue())
                .withColumnRenamed(WAY_ID, FieldNames.ID.getValue());

        final Dataset<Row> withWayNodes = joined.withColumn(NODE,
                struct(WayNodeFieldNames.NODEID.getValue(), WayNodeFieldNames.INDEX.getValue(),
                        FieldNames.Node.LATITUDE.getValue(), FieldNames.Node.LONGITUDE.getValue()));
        return withWayNodes.groupBy(FieldNames.ID.getValue())
                .agg(collect_list(NODE).as(FieldNames.Way.NODES.getValue()));
    }
}
