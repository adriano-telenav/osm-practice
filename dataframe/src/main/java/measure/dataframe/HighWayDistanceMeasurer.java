package measure.dataframe;


import function.filter.UntypedHighwayFilter;


public class HighWayDistanceMeasurer extends DataframeWayDistanceMeasurer{

    public HighWayDistanceMeasurer() {
        super(new UntypedHighwayFilter());
    }
}
