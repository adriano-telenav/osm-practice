package function;

import entity.complete.CompleteWay;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF1;
import scala.collection.JavaConversions;
import scala.collection.mutable.WrappedArray;

import java.util.List;
import java.util.stream.Collectors;



public class UntypedWayDistance implements UDF1<WrappedArray<Row>, Double> {

    @Override
    public Double call(WrappedArray<Row> nodesArray) {
        final List<Row> rows = JavaConversions.seqAsJavaList(nodesArray);
        final List<CompleteWay.Node> nodes =
                rows.stream().map(CompleteWay.Node::fromRow).collect(Collectors.toList());
        CompleteWay completeWay = new CompleteWay();
        completeWay.setNodes(nodes);

        return new TypedWayDistance().call(completeWay);
    }
}