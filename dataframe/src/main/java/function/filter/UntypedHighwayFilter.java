package function.filter;

import field.FieldNames;
import field.TagFieldNames;
import field.TagName;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import scala.collection.JavaConversions;

import java.util.ArrayList;
import java.util.List;

public class UntypedHighwayFilter implements FilterFunction<Row> {

    @Override
    public boolean call(Row row) {
        List<String> highwayTypes = new ArrayList<>();
        highwayTypes.add(TagName.Value.Highway.MOTORWAY.getValue());
        highwayTypes.add(TagName.Value.Highway.TRUNK.getValue());

        final List<GenericRowWithSchema> list = JavaConversions.seqAsJavaList(row.getAs(FieldNames.TAGS.getValue()));
        return list.stream()
                .anyMatch(tag -> tag.getAs(TagFieldNames.KEY.getValue()).equals(TagName.Key.HIGHWAY.getValue())
                        && highwayTypes.contains(tag.getAs(TagFieldNames.VALUE.getValue())));
    }
}
