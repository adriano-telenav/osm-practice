package function.filter;

import field.FieldNames;
import field.TagFieldNames;
import field.TagName;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import scala.collection.JavaConversions;

import java.util.List;

public class UntypedNamedRoadRelationFilter implements FilterFunction<Row> {

    @Override
    public boolean call(final Row row) {
        final List<GenericRowWithSchema> tags = JavaConversions.seqAsJavaList(row.getAs(FieldNames.TAGS.getValue()));
        final boolean isNamed = tags.stream().anyMatch(
                UntypedNamedRoadFilter::isNamed);

        boolean hasRoadRoute = tags.stream().anyMatch(
                tag -> tag.getAs(TagFieldNames.KEY.getValue()).equals(TagName.Key.ROUTE.getValue()) && tag
                        .getAs(TagFieldNames.VALUE.getValue()).equals(TagName.Value.NamedRoad.ROAD.getValue()));
        boolean hasRouteType = tags.stream().anyMatch(
                tag -> tag.getAs(TagFieldNames.KEY.getValue()).equals(TagName.Key.TYPE.getValue()) && tag
                        .getAs(TagFieldNames.VALUE.getValue()).equals(TagName.Value.NamedRoad.ROUTE.getValue()));
        final boolean isRoad = hasRoadRoute && hasRouteType;

        return isNamed && isRoad;
    }
}
