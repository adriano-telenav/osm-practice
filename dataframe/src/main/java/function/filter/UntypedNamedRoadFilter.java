package function.filter;

import field.FieldNames;
import field.TagFieldNames;
import field.TagName;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import scala.collection.JavaConversions;

import java.util.List;


public class UntypedNamedRoadFilter implements FilterFunction<Row> {

    @Override
    public boolean call(final Row row) {
        final List<GenericRowWithSchema> list = JavaConversions.seqAsJavaList(row.getAs(FieldNames.TAGS.getValue()));

        boolean isRoad =
                list.stream().anyMatch(tag -> tag.getAs(TagFieldNames.KEY.getValue())
                        .equals(TagName.Key.HIGHWAY.getValue()));
        boolean isNamed = list.stream()
                .anyMatch(UntypedNamedRoadFilter::isNamed);
        return isRoad && isNamed;
    }

    static boolean isNamed(final GenericRowWithSchema tag) {
        return tag.getAs(TagFieldNames.KEY.getValue()).equals(TagName.Key.NAME.getValue()) || tag
                .getAs(TagFieldNames.KEY.getValue()).equals(TagName.Key.REF.getValue());
    }

}
