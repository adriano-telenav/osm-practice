package function.filter;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Row;

import static field.RelationMemberNames.*;
import static field.RelationMemberNames.Types.WAY;


public class WayInRelationFilter implements FilterFunction<Row> {

    @Override
    public boolean call(final Row row) {
        return row.getAs(TYPE.getValue()).equals(WAY.getValue());
    }
}
