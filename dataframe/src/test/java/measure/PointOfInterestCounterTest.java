package measure;

import base.BaseTest;
import entity.Tag;
import field.TagName;
import metric.counter.PointOfInterestCounter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class PointOfInterestCounterTest extends BaseTest {

    @Test
    public void countPointsOfInterestWithoutTags() {
        final Dataset<Row> dfWithoutPois = getTaggedDataframe(new Tag[] {});
        final long countedPointsOfinterest = PointOfInterestCounter.count(dfWithoutPois);
        assertEquals(countedPointsOfinterest, 0);
    }

    @Test
    public void countPointsOfInterestWithTags() {
        final Dataset<Row> dfWithPois = getTaggedDataframe(
                new Tag[] { new Tag("amenity", "bar"), new Tag("shop", "sports"), new Tag("historic", "building") });
        final long countedPointsOfinterest = PointOfInterestCounter.count(dfWithPois);
        assertEquals(countedPointsOfinterest, 3);
    }

    @Test
    public void countNonPointsOfInterest() {
        final Dataset<Row> dfWithPois = getTaggedDataframe(
                new Tag[] { new Tag(TagName.Key.HIGHWAY.getValue(), "motorway"),
                        new Tag(TagName.Key.HIGHWAY.getValue(), "secondary") });
        final long countedPointsOfinterest = PointOfInterestCounter.count(dfWithPois);
        assertEquals(countedPointsOfinterest, 0);
    }
}