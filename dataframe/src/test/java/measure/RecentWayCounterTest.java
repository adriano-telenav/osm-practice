package measure;

import base.BaseTest;
import metric.counter.RecentWayCounter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class RecentWayCounterTest extends BaseTest {

    @Test
    public void countRecentlyAddedWays() {
        final Long now = System.currentTimeMillis();
        final Dataset<Row> recentlyAdded = getTimedDataframe(new Long[] { now, now, 0L});
        final long countedRecentWays = RecentWayCounter.count(recentlyAdded);
        assertEquals(countedRecentWays, 2);
    }

    @Test
    public void countRecentAndNonRecentWays() {
        final Long now = System.currentTimeMillis();
        final long twoDaysAgo = getDaysAgoMillis(2);
        final long fiveDaysAgo = getDaysAgoMillis(5);
        final long tenDaysAgo = getDaysAgoMillis(10);
        final Dataset<Row> recentlyAdded = getTimedDataframe(new Long[] { now, twoDaysAgo, fiveDaysAgo, tenDaysAgo });

        final long countedRecentWays = RecentWayCounter.count(recentlyAdded);
        assertEquals(countedRecentWays, 3);
    }
}