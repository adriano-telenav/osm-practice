package measure;

import base.BaseTest;
import measure.dataframe.NamedRoadDistanceMeasurer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class NamedRoadDistanceMeasurerTest extends BaseTest {

    private static final String TEST_NODES_PATH = "a3-turda.osm.pbf.node.parquet";
    private static final String TEST_WAYS_PATH = "a3-turda.osm.pbf.way.parquet";
    private static final String TEST_RELATIONS_PATH = "a3-turda.osm.pbf.relation.parquet";

    @Test
    public void measureDataframeWays() {
        final Dataset<Row> nodes = readTestFileIntoDataframe(TEST_NODES_PATH);
        final Dataset<Row> ways = readTestFileIntoDataframe(TEST_WAYS_PATH);
        final double measure = new NamedRoadDistanceMeasurer().measure(nodes, ways);
        assertTrue(63 < measure && measure < 64);
    }

    @Test
    public void measureRelations() {
        final Dataset<Row> relations = readTestFileIntoDataframe(TEST_RELATIONS_PATH);
        final Dataset<Row> ways = readTestFileIntoDataframe(TEST_WAYS_PATH);
        final Dataset<Row> nodes = readTestFileIntoDataframe(TEST_NODES_PATH);
        final double namedRoadRelationDistance = new NamedRoadDistanceMeasurer()
                .measureRoadsInRelations(nodes, ways, relations);
        System.out.println(namedRoadRelationDistance);
    }
}
