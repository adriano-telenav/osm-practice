package measure;

import base.BaseTest;
import entity.Tag;
import field.TagName;
import metric.counter.HighWayCounter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


public class HighWayCounterTest extends BaseTest {

    @Test
    public void countHighwaysWithoutTags() {
        final Dataset<Row> dfWithoutHighways = getTaggedDataframe(new Tag[] {});
        final long countedHighways = HighWayCounter.count(dfWithoutHighways);
        assertEquals(countedHighways, 0);
    }

    @Test
    public void countHighwaysWithTags() {
        final Dataset<Row> dfWithHighways = getTaggedDataframe(
                new Tag[] { new Tag(TagName.Key.HIGHWAY.getValue(), "motorway"),
                        new Tag(TagName.Key.HIGHWAY.getValue(), ""),
                        new Tag(TagName.Key.HIGHWAY.getValue(), "trunk"),
                        new Tag(TagName.Key.HIGHWAY.getValue(), "trunk"),
                        new Tag(TagName.Key.HIGHWAY.getValue(), "primary"),
                        new Tag(TagName.Key.HIGHWAY.getValue(), "secondary")});
        final long countedHighways = HighWayCounter.count(dfWithHighways);
        assertEquals(countedHighways, 3);
    }

    @Test
    public void countNonHighways() {
        final Dataset<Row> dfWithHighways = getTaggedDataframe(
                new Tag[] {new Tag(TagName.Key.HIGHWAY.getValue(), "primary"),
                        new Tag(TagName.Key.HIGHWAY.getValue(), "secondary")});
        final long countedHighways = HighWayCounter.count(dfWithHighways);
        assertEquals(countedHighways, 0);
    }
}