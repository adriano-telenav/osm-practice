package flatten;

import base.BaseTest;
import entity.raw.Relation;
import entity.Tag;
import entity.raw.Way;
import entity.raw.Way.Node;
import field.*;
import metric.flatten.Flattener;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.IntegerType;
import org.apache.spark.sql.types.LongType;
import org.apache.spark.sql.types.StringType;
import org.apache.spark.sql.types.StructField;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;


public class FlattenerTest extends BaseTest {


    @Test
    public void flattenTags() {
        final Dataset<Row> ways = getTaggedDataframe(
                new Tag[] { new Tag(TagName.Key.HIGHWAY.getValue(), "motorway"),
                        new Tag(TagName.Key.HIGHWAY.getValue(), ""),
                        new Tag(TagName.Key.HIGHWAY.getValue(), "trunk")
                });
        Dataset<Row> flattenedWays = Flattener.flattenTags(ways);

        StructField[] fields = flattenedWays.schema().fields();
        boolean containsKeyColumn = Arrays.stream(fields)
                .anyMatch(r -> r.name().equals(TagFieldNames.KEY.getValue()) && r.dataType() instanceof StringType);
        boolean containsValueColumn = Arrays.stream(fields)
                .anyMatch(r -> r.name().equals(TagFieldNames.VALUE.getValue()) && r.dataType() instanceof StringType);
        boolean containsTagsColumn = Arrays.stream(fields).anyMatch(r -> r.name().equals(FieldNames.TAGS.getValue()));

        assertTrue(containsKeyColumn);
        assertTrue(containsValueColumn);
        assertFalse(containsTagsColumn);
    }

    @Test
    public void flattenNodes() {
        List<Way> ways = new ArrayList<>();
        Way first = new Way();
        List<Node> firstNodes = new ArrayList<>();
        firstNodes.add(new Node(0L,0));
        firstNodes.add(new Node(1L,1));
        firstNodes.add(new Node(2L,2));
        first.setId(1L);
        first.setNodes(firstNodes);
        ways.add(first);

        Way second = new Way();
        List<Node> secondNodes = new ArrayList<>();
        secondNodes.add(new Node(0L, 0));
        secondNodes.add(new Node(1L, 1));
        secondNodes.add(new Node(2L, 2));
        second.setId(2L);
        second.setNodes(secondNodes);
        ways.add(second);

        Dataset<Row> waysWithNodes = getWayWithNodes(ways);
        final Dataset<Row> flattened = Flattener.flattenNodes(waysWithNodes);

        StructField[] fields = flattened.schema().fields();
        boolean containsNodeIdColumn = Arrays.stream(fields).anyMatch(
                r -> r.name().equals(WayNodeFieldNames.NODEID.getValue()) && r.dataType() instanceof LongType);
        boolean containsIndexColumn = Arrays.stream(fields).anyMatch(
                r -> r.name().equals(WayNodeFieldNames.INDEX.getValue()) && r.dataType() instanceof IntegerType);
        boolean containsNodeColumn = Arrays.stream(fields).anyMatch(r -> r.name().equals(FieldNames.Way.NODES.getValue()));

        assertTrue(containsNodeIdColumn);
        assertTrue(containsIndexColumn);
        assertFalse(containsNodeColumn);
    }
    @Test
    public void flattenRelation() {
        List<Relation> relations = new ArrayList<>();
        Relation first = new Relation();
        List<Relation.Member> firstMembers = new ArrayList<>();
        firstMembers.add(new Relation.Member(1L, "", ""));
        firstMembers.add(new Relation.Member(2L,"",""));
        first.setId(1L);
        first.setMembers(firstMembers);
        relations.add(first);

        Relation second = new Relation();
        List<Relation.Member> secondMembers = new ArrayList<>();
        secondMembers.add(new Relation.Member(0L, "",""));
        second.setId(2L);
        second.setMembers(secondMembers);
        relations.add(second);

        Dataset<Row> waysWithNodes = getRelations(relations);
        final Dataset<Row> flattened = Flattener.flattenRelations(waysWithNodes);

        StructField[] fields = flattened.schema().fields();
        boolean containsIdColumn = Arrays.stream(fields).anyMatch(
                r -> r.name().equals(RelationMemberNames.ID.getValue()) && r.dataType() instanceof LongType);
        boolean containsTypeColumn = Arrays.stream(fields).anyMatch(
                r -> r.name().equals(RelationMemberNames.TYPE.getValue()) && r.dataType() instanceof StringType);
        boolean containsRoleColumn = Arrays.stream(fields).anyMatch(
                r -> r.name().equals(RelationMemberNames.ROLE.getValue()) && r.dataType() instanceof StringType);
        boolean containsMemberColumn = Arrays.stream(fields).anyMatch(r -> r.name().equals(FieldNames.Relation.MEMBERS.getValue()));

        assertTrue(containsIdColumn);
        assertTrue(containsTypeColumn);
        assertTrue(containsRoleColumn);
        assertFalse(containsMemberColumn);
    }

}
