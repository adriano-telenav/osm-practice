package base;

import entity.Tag;
import entity.raw.Relation;
import entity.raw.Way;
import field.TagFieldNames;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Before;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static field.FieldNames.*;
import static org.apache.spark.sql.types.DataTypes.*;


public class BaseTest {

    private SparkSession spark;

    @Before
    public void setUp() {
        final SparkConf conf = new SparkConf().setAppName("testing")
                .setMaster("local[*]")
                .set("spark.sql.parquet.binaryAsString", "true");
        this.spark = new SparkSession.Builder().config(conf).getOrCreate();
    }

    protected Dataset<Row> readTestFileIntoDataframe(String filename) {
        return spark.read().parquet("../src/test/resources/" + filename);
    }

    protected Dataset<Row> getTaggedDataframe(final Tag[] tags) {
        int i = 3;
        List<Row> rows = new ArrayList<>();
        rows.add(RowFactory.create(1, new ArrayList<>()));
        rows.add(RowFactory.create(2, new ArrayList<>()));

        for (Tag tag : tags) {
            rows.add(RowFactory.create(i++, new Row[] { RowFactory.create(tag.getKey(), tag.getValue()), }));
        }
        return spark.createDataFrame(rows, getSchemaWithTag());
    }

    private static StructType getSchemaWithTag() {
        StructType tagStruct = createStructType(
                new StructField[] { createStructField(TagFieldNames.KEY.getValue(), StringType, false),
                        createStructField(TagFieldNames.VALUE.getValue(), StringType, false) });

        return DataTypes.createStructType(new StructField[] {
                createStructField(ID.getValue(), IntegerType, false),
                createStructField(TAGS.getValue(), createArrayType(tagStruct), false) });
    }

    protected Dataset<Row> getWayWithNodes(final List<Way> ways) {
        final Dataset<Way> dataset = spark.createDataset(ways, Encoders.bean(Way.class));
        return dataset.toDF();
    }

    protected Dataset<Row> getRelations(final List<Relation> relations) {
        final Dataset<Relation> dataset = spark.createDataset(relations, Encoders.bean(Relation.class));
        return dataset.toDF();
    }
    protected Dataset<Row> getTimedDataframe(final Long[] timestamps) {
        int i = 3;
        List<Row> rows = new ArrayList<>();
        rows.add(RowFactory.create(1, 0L));
        rows.add(RowFactory.create(2, 0L));

        for (Long timestamp : timestamps) {
            rows.add(RowFactory.create(i++, timestamp));
        }
        return spark.createDataFrame(rows, DataTypes.createStructType(
                new StructField[] { createStructField(ID.getValue(), IntegerType, false),
                        createStructField(TIMESTAMP.getValue(), LongType, false) }));
    }

    protected long getDaysAgoMillis(int days) {
        return LocalDateTime.now().minusDays(days).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
}
