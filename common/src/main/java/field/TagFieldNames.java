package field;

public enum TagFieldNames {
    KEY("key"),
    VALUE("value");

    private String value;

    TagFieldNames(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
