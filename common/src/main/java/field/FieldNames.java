package field;

public enum FieldNames {
    ID("id"),
    VERSION("version"),
    TIMESTAMP("timestamp"),
    CHANGESET("changeset"),
    UID("uid"),
    USER_SID("user_sid"),
    TAGS("tags");

    private String value;

    FieldNames(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public enum Node {
        LATITUDE("latitude"),
        LONGITUDE("longitude");

        private String value;

        Node(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum Way {
        NODES("nodes");

        private String value;

        Way(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum Relation {
        MEMBERS("members");

        private String value;

        Relation(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
