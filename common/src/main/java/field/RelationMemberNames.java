package field;

public enum  RelationMemberNames {
    ID("id"), ROLE("role"), TYPE("type");

    private final String value;

    RelationMemberNames(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public enum Types {
        WAY("Way");

        private String value;

        Types(final String value) {

            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
