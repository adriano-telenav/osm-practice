package field;

public class TagName {

    public enum Key {
        HIGHWAY("highway"),
        NAME("name"),
        REF("ref"),
        ROUTE("route"),
        TYPE("type");

        private final String value;

        Key(final String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


    public static class Value {

        public enum Highway {
            MOTORWAY("motorway"),
            TRUNK("trunk");

            private final String value;

            Highway(final String value) {
                this.value = value;
            }

            public String getValue() {
                return value;
            }
        }


        public enum NamedRoad {
            ROAD("road"),
            ROUTE("route");

            private final String value;

            NamedRoad(final String value) {
                this.value = value;
            }

            public String getValue() {
                return value;
            }
        }

    }

}
