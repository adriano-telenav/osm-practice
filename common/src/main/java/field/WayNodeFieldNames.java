package field;

public enum WayNodeFieldNames {
    INDEX("index"),
    NODEID("nodeId");

    private String value;

    WayNodeFieldNames(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
