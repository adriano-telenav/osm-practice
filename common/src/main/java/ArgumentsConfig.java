import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isBlank;


public class ArgumentsConfig {

    private static final String MASTER = "master";
    private static final String NODES_PATH = "nodes_path";
    private static final String WAYS_PATH = "ways_path";
    private static final String RELATIONS_PATH = "relations_path";
    private final Map<String, String> passedArguments;

    public ArgumentsConfig(final String[] args) throws Exception {
        passedArguments = Arrays.stream(args)
                .map(argument -> argument.split("="))
                .filter(argument -> argument.length == 2)
                .collect(Collectors.toMap(argument -> argument[0].trim(), el -> el[1].trim()));
        final String message = getConfigErrors(passedArguments);
        if (!isBlank(message)) {
            throw new Exception(message);
        }
    }

    private String getConfigErrors(final Map<String, String> passedArguments) {
        String message = "";
        if (isBlank(passedArguments.get(MASTER))) {
            message += " Invalid or missing master.";
        }
        if (isBlank(passedArguments.get(NODES_PATH))) {
            message += " Invalid or missing nodes path.";
        }
        if (isBlank(passedArguments.get(WAYS_PATH))) {
            message += " Invalid or missing ways path.";
        }
        return message;
    }

    public String getMaster() {
        return passedArguments.get(MASTER);
    }

    public String getNodesPath() {
        return passedArguments.get(NODES_PATH);
    }

    public String getWaysPath() {
        return passedArguments.get(WAYS_PATH);
    }

    public String getRelationsPath() {
        return passedArguments.get(RELATIONS_PATH);
    }
}
