package entity.raw;

import entity.Tag;

import java.io.Serializable;
import java.util.List;


public class Relation {
    private long id;
    private List<Tag> tags;
    private List<Member> members;

    public Relation() {
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(final List<Tag> tags) {
        this.tags = tags;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(final List<Member> members) {
        this.members = members;
    }

    public static class Member implements Serializable {
        private long id;
        private String role;
        private String type;

        public Member() {
        }

        public Member(final long id, final String role, final String type) {
            this.id = id;
            this.role = role;
            this.type = type;
        }

        public long getId() {
            return id;
        }

        public void setId(final long id) {
            this.id = id;
        }

        public String getRole() {
            return role;
        }

        public void setRole(final String role) {
            this.role = role;
        }

        public String getType() {
            return type;
        }

        public void setType(final String type) {
            this.type = type;
        }
    }
}
