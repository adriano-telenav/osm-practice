package entity.raw;

import entity.Tag;

import java.io.Serializable;
import java.util.List;


public class Way implements Serializable {
    private long id;
    private List<Node> nodes;
    private List<Tag> tags;

    public Way() {
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(final List<Node> nodes) {
        this.nodes = nodes;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(final List<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "CompleteWay{" + "id=" + id + ", nodes=" + nodes + '}';
    }

    public static class Node implements Serializable {
        private Long nodeId;
        private Integer index;

        public Node(final Long nodeId, final Integer index) {
            this.nodeId = nodeId;
            this.index = index;
        }

        public Node() {}

        public Long getNodeId() {
            return nodeId;
        }

        public void setNodeId(final Long nodeId) {
            this.nodeId = nodeId;
        }

        public Integer getIndex() {
            return index;
        }

        public void setIndex(final Integer index) {
            this.index = index;
        }
    }
}
