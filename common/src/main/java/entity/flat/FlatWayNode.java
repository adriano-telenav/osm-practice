package entity.flat;

import java.io.Serializable;


public class FlatWayNode implements Serializable {
    private Long wayId;
    private Long nodeId;
    private Integer index;

    public FlatWayNode() {
    }

    public FlatWayNode(final long id, final Long nodeId, final Integer index) {
        this.wayId = id;
        this.nodeId = nodeId;
        this.index = index;
    }

    public Long getWayId() {
        return wayId;
    }

    public void setWayId(final Long wayId) {
        this.wayId = wayId;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(final Long nodeId) {
        this.nodeId = nodeId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }
}
