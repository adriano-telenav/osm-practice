package entity.flat;

import java.io.Serializable;


public class FlatRelationWay implements Serializable {
    private Long relationId;
    private String role;
    private Long wayId;
    private String type;

    public FlatRelationWay() {
    }

    private FlatRelationWay(final Builder builder) {
        setRelationId(builder.relationId);
        setRole(builder.role);
        setWayId(builder.wayId);
        setType(builder.type);
    }

    public static Builder builder() {
        return new Builder();
    }


    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(final Long relationId) {
        this.relationId = relationId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(final String role) {
        this.role = role;
    }

    public Long getWayId() {
        return wayId;
    }

    public void setWayId(final Long wayId) {
        this.wayId = wayId;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public static final class Builder {

        private Long relationId;
        private String role;
        private Long wayId;
        private String type;

        private Builder() {
        }

        public Builder withRelationId(final Long val) {
            relationId = val;
            return this;
        }

        public Builder withRole(final String val) {
            role = val;
            return this;
        }

        public Builder withId(final Long val) {
            wayId = val;
            return this;
        }

        public Builder withType(final String val) {
            type = val;
            return this;
        }

        public FlatRelationWay build() {
            return new FlatRelationWay(this);
        }
    }
}
