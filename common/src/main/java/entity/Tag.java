package entity;

import java.io.Serializable;


public class Tag implements Serializable {
    private String key;
    private String value;

    public Tag() {
    }

    public Tag(final String key, final String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
