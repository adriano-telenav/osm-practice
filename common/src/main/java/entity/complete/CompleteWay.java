package entity.complete;


import entity.flat.FlatWayNode;
import field.FieldNames;
import field.WayNodeFieldNames;
import org.apache.spark.sql.Row;
import scala.Tuple2;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;


public class CompleteWay implements Serializable {

    private long wayId;
    private List<Node> nodes;

    public CompleteWay() {
    }

    public long getWayId() {
        return wayId;
    }

    public void setWayId(final long wayId) {
        this.wayId = wayId;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(final List<Node> nodes) {
        this.nodes = nodes;
    }

    @Override
    public String toString() {
        return "CompleteWay{" + "wayId=" + wayId + ", nodes=" + nodes + '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final CompleteWay that = (CompleteWay) o;
        return wayId == that.wayId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(wayId);
    }

    public static class Node implements Comparable<Node>, Serializable {

        static final double EARTH_RADIUS = 6371;
        private Long nodeId;
        private Integer index;
        private Double latitude;
        private Double longitude;

        public Node() {
        }

        public static Node fromTuple(final Tuple2<FlatWayNode, entity.raw.Node> tuple) {
            Node node = new Node();
            node.nodeId = tuple._1.getNodeId();
            node.index = tuple._1.getIndex();
            node.longitude = tuple._2.getLongitude();
            node.latitude = tuple._2.getLatitude();
            return node;
        }

        public static Node fromRow(Row row) {
            final Node node = new Node();
            node.setNodeId(row.getAs(WayNodeFieldNames.NODEID.getValue()));
            node.setIndex(row.getAs(WayNodeFieldNames.INDEX.getValue()));
            node.setLatitude(row.getAs(FieldNames.Node.LATITUDE.getValue()));
            node.setLongitude(row.getAs(FieldNames.Node.LONGITUDE.getValue()));
            return node;
        }

        public Long getNodeId() {
            return nodeId;
        }

        public void setNodeId(final Long nodeId) {
            this.nodeId = nodeId;
        }

        public Integer getIndex() {
            return index;
        }

        public void setIndex(final Integer index) {
            this.index = index;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(final Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(final Double longitude) {
            this.longitude = longitude;
        }

        public double distanceToInKM(final Node destination) {
            final double latDistance = Math.toRadians(latitude - destination.latitude);
            final double lonDistance = Math.toRadians(longitude - destination.longitude);
            final double inRadians = Math.pow(Math.sin(latDistance / 2), 2) + Math.cos(Math.toRadians(latitude)) *
                    Math.cos(Math.toRadians(destination.latitude)) * Math.pow(Math.sin(lonDistance / 2), 2);
            final double sphericalCosine = 2 * Math.atan2(Math.sqrt(inRadians), Math.sqrt(1 - inRadians));
            return EARTH_RADIUS * sphericalCosine;
        }

        public int compareTo(final Node wayNode) {
            return this.index - wayNode.index;
        }

        @Override
        public String toString() {
            return "Node{" + "nodeId=" + nodeId + ", index=" + index + ", latitude=" + latitude + ", longitude="
                    + longitude + '}';
        }


    }
}
