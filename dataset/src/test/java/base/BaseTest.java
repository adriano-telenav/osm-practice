package base;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Before;


public class BaseTest {
    private SparkSession spark;

    @Before
    public void setUp() {
        final SparkConf conf = new SparkConf().setAppName("testing")
                .setMaster("local[*]")
                .set("spark.sql.parquet.binaryAsString", "true");
        this.spark = new SparkSession.Builder().config(conf).getOrCreate();
    }

    protected <T> Dataset<T> readTestFileIntoDataset(String filename, Class<T> clazz) {
        final Dataset<Row> read = spark.read().parquet("../src/test/resources/" + filename);
        return read.as(Encoders.bean(clazz));
    }
}
