package measure;

import base.BaseTest;
import entity.raw.Node;
import entity.raw.Way;
import metric.measure.HighWayDistanceMeasurer;
import org.apache.spark.sql.Dataset;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class HighwayDistanceMeasurerTest extends BaseTest {
    private static final String HIGHWAY_NODES_PATH = "a3-turda.osm.pbf.node.parquet";
    private static final String HIGHWAY_WAYS_PATH = "a3-turda.osm.pbf.way.parquet";

    @Test
    public void measureDS() {
        final Dataset<Node> nodes = readTestFileIntoDataset(HIGHWAY_NODES_PATH, Node.class);
        final Dataset<Way> ways = readTestFileIntoDataset(HIGHWAY_WAYS_PATH, Way.class);
        final double measure = new HighWayDistanceMeasurer().measure(nodes, ways);
        assertTrue(15 < measure && measure < 16);
    }
}
