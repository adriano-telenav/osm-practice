package measure;

import base.BaseTest;
import entity.raw.Node;
import entity.raw.Relation;
import entity.raw.Way;
import metric.measure.NamedRoadDistanceMeasurer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class NamedRoadDistanceMeasurerTest extends BaseTest {

    private static final String TEST_NODES_PATH = "a3-turda.osm.pbf.node.parquet";
    private static final String TEST_WAYS_PATH = "a3-turda.osm.pbf.way.parquet";
    private static final String TEST_RELATIONS_PATH = "a3-turda.osm.pbf.relation.parquet";

    @Test
    public void measureDatasetWays() {
        final Dataset<Node> nodes = readTestFileIntoDataset(TEST_NODES_PATH, Node.class);
        final Dataset<Way> ways = readTestFileIntoDataset(TEST_WAYS_PATH, Way.class);
        final double measure = new NamedRoadDistanceMeasurer().measure(nodes, ways);
        assertTrue(63 < measure && measure < 64);
    }

    @Test
    public void measureRelationsDS() {
        final Dataset<Relation> relations = readTestFileIntoDataset(TEST_RELATIONS_PATH, Relation.class)
                .as(Encoders.bean(Relation.class));
        final Dataset<Way> ways = readTestFileIntoDataset(TEST_WAYS_PATH, Way.class)
                .as(Encoders.bean(Way.class));
        final Dataset<Node> nodes = readTestFileIntoDataset(TEST_NODES_PATH, Node.class)
                .as(Encoders.bean(Node.class));
        final double measure = new NamedRoadDistanceMeasurer()
                .measureRoadsInRelations(nodes, ways, relations);
        assertTrue(63 < measure && measure < 64);
    }

}
