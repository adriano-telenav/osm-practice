package function;

import entity.complete.CompleteWay;
import org.apache.spark.api.java.function.MapFunction;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class TypedWayDistance implements MapFunction<CompleteWay, Double> {

    @Override
    public Double call(final CompleteWay way) {
        double result = computeWayDistance(way);
        return result;
    }

    public static double computeWayDistance(final CompleteWay way) {
        final List<CompleteWay.Node> wayNodes = way.getNodes();
        Collections.sort(wayNodes);
        final Iterator<CompleteWay.Node> iterator = wayNodes.iterator();
        CompleteWay.Node prev = iterator.next();
        CompleteWay.Node current;
        double result = 0.0;
        while (iterator.hasNext()) {
            current = iterator.next();
            result += current.distanceToInKM(prev);
            prev = current;
        }
        return result;
    }
}
