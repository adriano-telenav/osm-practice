package function.filter;

import entity.Tag;
import entity.raw.Way;
import field.TagName;
import org.apache.spark.api.java.function.FilterFunction;

import java.util.List;


public class TypedNamedRoadFilter implements FilterFunction<Way> {

    @Override
    public boolean call(final Way way) {
        final List<Tag> tags = way.getTags();
        boolean isRoad = tags.stream().anyMatch(tag -> tag.getKey().equals(TagName.Key.HIGHWAY.getValue()));
        boolean isNamed = tags.stream().anyMatch(this::isNamed);
        return isRoad && isNamed;
    }

    private boolean isNamed(final Tag tag) {
        return (tag.getKey().equals(TagName.Key.NAME.getValue())) || (tag.getKey().equals(TagName.Key.REF.getValue()));
    }
}