package function.filter;

import entity.Tag;
import entity.raw.Relation;
import field.TagName;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.function.FilterFunction;

import java.util.List;


public class TypedRelationNamedRoadFilter implements FilterFunction<Relation> {

    @Override
    public boolean call(final Relation relation) {
        final List<Tag> tags = relation.getTags();
        boolean isNamed = tags.stream().anyMatch(this::isNamed);

        boolean hasRoadRoute = tags.stream().anyMatch(
                tag -> tag.getKey().equals(TagName.Key.ROUTE.getValue()) && tag.getValue()
                        .equals(TagName.Value.NamedRoad.ROAD.getValue()));
        boolean hasRouteType = tags.stream().anyMatch(
                tag -> tag.getKey().equals(TagName.Key.TYPE.getValue()) && tag.getValue()
                        .equals(TagName.Value.NamedRoad.ROUTE.getValue()));
        final boolean isRoad = hasRoadRoute && hasRouteType;
        return isNamed && isRoad;
    }

    private boolean isNamed(final Tag tag) {
        return (tag.getKey().equals(TagName.Key.NAME.getValue()) && StringUtils.isNotBlank(tag.getValue())) || (
                tag.getKey().equals(TagName.Key.REF.getValue()) && StringUtils.isNotBlank(tag.getValue()));
    }
}
