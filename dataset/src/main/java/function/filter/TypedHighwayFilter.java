package function.filter;

import entity.Tag;
import entity.raw.Way;
import field.TagName;
import org.apache.spark.api.java.function.FilterFunction;

import java.util.List;


public class TypedHighwayFilter implements FilterFunction<Way> {

    @Override
    public boolean call(final Way way) {
        final List<Tag> list = way.getTags();
        return list.stream().anyMatch(tag -> tag.getKey().equals(TagName.Key.HIGHWAY.getValue())
                && (tag.getValue().equals(TagName.Value.Highway.MOTORWAY.getValue())
                || tag.getValue().equals(TagName.Value.Highway.TRUNK.getValue())));
    }
}
