package function.flatten;

import entity.flat.FlatWayNode;
import entity.raw.Way;
import org.apache.spark.api.java.function.FlatMapFunction;

import java.util.Iterator;


public class WayNodeFlattenFunction implements FlatMapFunction<Way, FlatWayNode> {

    @Override
    public Iterator<FlatWayNode> call(final Way readWay)  {
        return readWay.getNodes().stream()
                .map(n -> new FlatWayNode(readWay.getId(), n.getNodeId(), n.getIndex()))
                .iterator();
    }
}
