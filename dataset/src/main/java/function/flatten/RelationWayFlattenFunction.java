package function.flatten;

import entity.flat.FlatRelationWay;
import entity.raw.Relation;
import org.apache.spark.api.java.function.FlatMapFunction;

import java.util.Iterator;


public class RelationWayFlattenFunction implements FlatMapFunction<Relation, FlatRelationWay> {

    @Override
    public Iterator<FlatRelationWay> call(final Relation relation) {
        return relation.getMembers().stream()
                .map(member -> FlatRelationWay.builder()
                        .withId(member.getId())
                        .withRelationId(relation.getId())
                        .withType(member.getType())
                        .withRole(member.getRole())
                        .build())
                .iterator();
    }
}
