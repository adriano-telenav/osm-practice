package function;

import entity.complete.CompleteWay;
import entity.flat.FlatWayNode;
import entity.raw.Node;
import org.apache.spark.api.java.function.MapGroupsFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class CompleteWayGroupMapper implements MapGroupsFunction<Long,Tuple2<FlatWayNode, Node>,CompleteWay> {

    @Override
    public CompleteWay call(final Long key, final Iterator<Tuple2<FlatWayNode, Node>> values) {
        CompleteWay completeWay = new CompleteWay();
        completeWay.setWayId(key);
        final List<CompleteWay.Node> nodes = new ArrayList<>();
        while (values.hasNext()) {
            final Tuple2<FlatWayNode, Node> tuple = values.next();
            final CompleteWay.Node node = CompleteWay.Node.fromTuple(tuple);
            nodes.add(node);
        }
        completeWay.setNodes(nodes);
        return completeWay;
    }
}
