import entity.raw.Node;
import entity.raw.Way;
import metric.measure.HighWayDistanceMeasurer;
import metric.measure.NamedRoadDistanceMeasurer;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;


public class OsmDatasetStatisticsJob {
    public static void main(String[] args) throws Exception {
        final ArgumentsConfig config = new ArgumentsConfig(args);
        final SparkConf conf = new SparkConf().setAppName("osm-practice").setMaster(config.getMaster())
                .set("spark.sql.parquet.binaryAsString", "true");
        final SparkSession spark = new SparkSession.Builder().config(conf).getOrCreate();

        final Dataset<Node> nodesDS = spark.read().parquet(config.getNodesPath()).as(Encoders.bean(Node.class));
        final Dataset<Way> waysDS = spark.read().parquet(config.getWaysPath()).as(Encoders.bean(Way.class));
        final double totalHighwayLength = new HighWayDistanceMeasurer().measure(nodesDS, waysDS);
        final NamedRoadDistanceMeasurer namedRoadDistanceMeasurer = new NamedRoadDistanceMeasurer();
        final double totalNamedRoadLength = namedRoadDistanceMeasurer.measure(nodesDS, waysDS);

        System.out.println("Total highway length: " + totalHighwayLength);
        System.out.println("Total named road length: " + totalNamedRoadLength);
    }
}