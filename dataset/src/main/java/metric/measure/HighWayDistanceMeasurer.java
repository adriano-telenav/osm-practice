package metric.measure;


import function.filter.TypedHighwayFilter;


public final class HighWayDistanceMeasurer extends DatasetWaysDistanceMeasurer {

    public HighWayDistanceMeasurer() {
        super(new TypedHighwayFilter());
    }
}