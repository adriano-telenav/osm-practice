package metric.measure;


import entity.complete.CompleteWay;
import entity.flat.FlatWayNode;
import entity.raw.Node;
import entity.raw.Way;
import field.FieldNames;
import field.WayNodeFieldNames;
import function.CompleteWayGroupMapper;
import function.TypedWayDistance;
import function.flatten.WayNodeFlattenFunction;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import scala.Tuple2;

import static org.apache.spark.sql.functions.sum;


public class DatasetWaysDistanceMeasurer {

    private static final String TOTAL = "total";
    protected static final String WAY_ID = "wayId";

    private final FilterFunction<Way> datasetFilter;

    public DatasetWaysDistanceMeasurer(final FilterFunction<Way> datasetFilter) {
        this.datasetFilter = datasetFilter;
    }

    public double measure(final Dataset<Node> nodes, final Dataset<Way> ways) {
        final Dataset<Way> filtered = ways.filter(datasetFilter);

        final Dataset<CompleteWay> nested = makeCompleteWays(nodes, filtered);
        final Dataset<Double> distances = nested.map(new TypedWayDistance(), Encoders.DOUBLE());
        return sumValues(distances);
    }

    protected Double sumValues(final Dataset<Double> distances) {
        return distances.agg(sum("value").as(TOTAL)).first().getAs(TOTAL);
    }

    protected Dataset<CompleteWay> makeCompleteWays(final Dataset<Node> nodes, final Dataset<Way> ways) {
        final Dataset<FlatWayNode> flatWays = ways
                .flatMap(new WayNodeFlattenFunction(), Encoders.bean(FlatWayNode.class));
        return flatWays.joinWith(nodes,
                nodes.col(FieldNames.ID.getValue()).equalTo(flatWays.col(WayNodeFieldNames.NODEID.getValue())))
                .groupByKey((MapFunction<Tuple2<FlatWayNode, Node>, Long>) r -> r._1.getWayId(), Encoders.LONG())
                .mapGroups(new CompleteWayGroupMapper(), Encoders.bean(CompleteWay.class));
    }
}
