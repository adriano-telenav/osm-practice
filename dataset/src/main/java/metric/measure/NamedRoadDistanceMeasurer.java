package metric.measure;

import entity.complete.CompleteWay;
import entity.flat.FlatRelationWay;
import entity.raw.Node;
import entity.raw.Relation;
import entity.raw.Way;
import function.TypedWayDistance;
import function.filter.TypedNamedRoadFilter;
import function.filter.TypedRelationNamedRoadFilter;
import function.flatten.RelationWayFlattenFunction;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import scala.Tuple2;

import static field.RelationMemberNames.Types.WAY;


public class NamedRoadDistanceMeasurer extends DatasetWaysDistanceMeasurer {

    public NamedRoadDistanceMeasurer() {
        super(new TypedNamedRoadFilter());
    }

    public double measureRoadsInRelations(final Dataset<Node> nodes, final Dataset<Way> ways,
            final Dataset<Relation> relations) {
        final Dataset<CompleteWay> namedRoadCompleteWays = getRoadCompleteWays(nodes, ways);
        final Dataset<CompleteWay> allCompleteWays = makeCompleteWays(nodes, ways);

        final Dataset<FlatRelationWay> waysInFlatRelation = getNamedRoadRelations(relations);
        final Dataset<CompleteWay> completeWaysInRelations =
                getCompleteWaysInRelations(waysInFlatRelation, allCompleteWays);

        final Dataset<CompleteWay> allNamedRoads = namedRoadCompleteWays.union(completeWaysInRelations).distinct();
        final Dataset<Double> wayDistances = allNamedRoads.map(new TypedWayDistance(), Encoders.DOUBLE());
        return sumValues(wayDistances);
    }

    private Dataset<FlatRelationWay> getNamedRoadRelations(final Dataset<Relation> relations) {
        final Dataset<Relation> namedRoadRelation = relations.filter(new TypedRelationNamedRoadFilter());
        final Dataset<FlatRelationWay> flatRelationWayDataset =
                namedRoadRelation.flatMap(new RelationWayFlattenFunction(), Encoders.bean(FlatRelationWay.class));

        return flatRelationWayDataset
                .filter((FilterFunction<FlatRelationWay>) flatRelationWay -> flatRelationWay.getType()
                        .equals(WAY.getValue()));
    }

    private Dataset<CompleteWay> getRoadCompleteWays(final Dataset<Node> nodes, final Dataset<Way> ways) {
        final Dataset<Way> namedRoadWays = ways.filter(new TypedNamedRoadFilter());
        return makeCompleteWays(nodes, namedRoadWays);
    }

    private Dataset<CompleteWay> getCompleteWaysInRelations(final Dataset<FlatRelationWay> waysInFlatRelation,
            final Dataset<CompleteWay> completeWays) {
        return waysInFlatRelation
                    .joinWith(completeWays, waysInFlatRelation.col(WAY_ID).equalTo(completeWays.col(WAY_ID)), "inner")
                    .map((MapFunction<Tuple2<FlatRelationWay, CompleteWay>, CompleteWay>) r -> r._2,
                            Encoders.bean(CompleteWay.class));
    }
}
