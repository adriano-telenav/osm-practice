package metric.counter;

import com.telenav.map.Filter;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.TagContainer;
import com.telenav.map.filter.TagFilter;
import entity.raw.Way;
import function.filter.TypedHighwayFilter;
import org.apache.spark.api.java.JavaRDD;


public class HighwayCounter {
    public static long count(final JavaRDD<Way> ways) {
        return ways.filter(way -> new TypedHighwayFilter().call(way)).count();
    }

    public static long countRawWays(final JavaRDD<RawWay> rawWays) {
        final Filter<TagContainer> highwayFilter = new TagFilter("highway", "motorway", "trunk");
        final JavaRDD<RawWay> highways = rawWays.filter(highwayFilter::accept);
        return highways.count();
    }
}
