import com.telenav.map.entity.RawWay;
import com.telenav.map.storage.read.ParquetReader;
import com.telenav.map.storage.read.RawRowToRawWayConverter;
import entity.raw.Way;
import metric.counter.HighwayCounter;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;


public class OsmRDDStatisticsJob {

    public static void main(String[] args) throws Exception {
        final ArgumentsConfig config = new ArgumentsConfig(args);
        final SparkConf conf = new SparkConf().setAppName("osm-practice").setMaster(config.getMaster())
                .set("spark.sql.parquet.binaryAsString", "true");

        final SparkSession spark = new SparkSession.Builder().config(conf).getOrCreate();
        final JavaRDD<Way> ways = spark.read().parquet(config.getWaysPath()).as(Encoders.bean(Way.class)).javaRDD();

        final JavaSparkContext javaSparkContext = JavaSparkContext.fromSparkContext(spark.sparkContext());
        final JavaRDD<RawWay> rawWays =
                new ParquetReader<>(javaSparkContext, new RawRowToRawWayConverter(), config.getWaysPath()).read();

        final long highwayNumber = HighwayCounter.count(ways);
        final long rawHighwayNumber = HighwayCounter.countRawWays(rawWays);

        System.out.println("Highway number: " + highwayNumber);
        System.out.println("Highway number: " + rawHighwayNumber);
    }

}
