package metric.counter;

import base.BaseTest;
import com.telenav.map.entity.RawWay;
import entity.Tag;
import entity.raw.Way;
import field.TagName;
import org.apache.spark.api.java.JavaRDD;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;


public class HighwayCounterTest extends BaseTest {

    @Test
    public void countHighwaysWithoutTags() {
        final Tag[] tags = {};
        final Way way = new Way();
        way.setTags(Arrays.asList(tags));
        final List<Way> ways = new ArrayList<>();
        ways.add(way);

        final JavaRDD<Way> rddFromList = getRDDFromList(ways);
        final long countedHighways = HighwayCounter.count(rddFromList);
        assertEquals(countedHighways, 0);
    }

    @Test
    public void countHighwaysWithTags() {
        final Tag[] tags = new Tag[] { new Tag(TagName.Key.HIGHWAY.getValue(), "motorway"),
                new Tag(TagName.Key.HIGHWAY.getValue(), ""),
                new Tag(TagName.Key.HIGHWAY.getValue(), "trunk"),
                new Tag(TagName.Key.HIGHWAY.getValue(), "trunk"),
                new Tag(TagName.Key.HIGHWAY.getValue(), "primary"),
                new Tag(TagName.Key.HIGHWAY.getValue(), "secondary") };

        final List<Way> ways = getWaysFromEachTag(tags);

        final JavaRDD<Way> waysRDD = getRDDFromList(ways);
        final long countedHighways = HighwayCounter.count(waysRDD);
        assertEquals(countedHighways, 3);
    }

    @Test
    public void countNonHighways() {
        final Tag[] tags = { new Tag(TagName.Key.HIGHWAY.getValue(), "primary"),
                new Tag(TagName.Key.HIGHWAY.getValue(), "secondary") };
        final List<Way> ways = getWaysFromEachTag(tags);

        final JavaRDD<Way> waysRDD = getRDDFromList(ways);
        final long countedHighways = HighwayCounter.count(waysRDD);
        assertEquals(countedHighways, 0);
    }

    private List<Way> getWaysFromEachTag(final Tag[] tags) {
        return Arrays.stream(tags).map(tag -> {
            final List<Tag> tagList = new ArrayList<>();
            tagList.add(tag);
            Way way = new Way();
            way.setTags(tagList);
            return way;
        }).collect(Collectors.toList());
    }


    @Test
    public void rawWaysCountWithoutTags() {
        final ArrayList<Long> nodes = new ArrayList<>();
        nodes.add(4L);
        RawWay rawWay= new RawWay(1, nodes);
        List<RawWay> rawWays = new ArrayList<>();
        rawWays.add(rawWay);
        final JavaRDD<RawWay> rawWaysRdd = getRDDFromList(rawWays);
        final long countedHighways = HighwayCounter.countRawWays(rawWaysRdd);
        assertEquals(countedHighways, 0);
    }


    @Test
    public void testRawCountNonHighWays() {
        final List<RawWay> rawWays = getRawWaysFromEachTag(new String[] { "primary", "secondary", "" });
        final JavaRDD<RawWay> rawWaysRdd = getRDDFromList(rawWays);
        final long countedHighways = HighwayCounter.countRawWays(rawWaysRdd);
        assertEquals(countedHighways, 0);
    }

    @Test
    public void rawWaysCountWithTags() {
        final List<RawWay> rawWays = getRawWaysFromEachTag(new String[] { "trunk", "motorway", "motorway", "primary"});
        final JavaRDD<RawWay> rawWaysRdd = getRDDFromList(rawWays);
        final long countedHighways = HighwayCounter.countRawWays(rawWaysRdd);
        assertEquals(countedHighways, 3);
    }

    private List<RawWay> getRawWaysFromEachTag(final String[] tags) {
        return Arrays.stream(tags).map(tag -> {
            final ArrayList<Long> nodes = new ArrayList<>();
            nodes.add(4L);
            Map<String, String> tagsMap = new HashMap<>();
            tagsMap.put("highway", tag);
            return new RawWay(1, nodes, tagsMap);
        }).collect(Collectors.toList());
    }
}