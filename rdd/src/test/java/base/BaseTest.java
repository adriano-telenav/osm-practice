package base;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.junit.Before;

import java.util.List;


public class BaseTest {

    private SparkSession spark;

    @Before
    public void setUp() {
        final SparkConf conf = new SparkConf().setAppName("testing")
                .setMaster("local[*]")
                .set("spark.sql.parquet.binaryAsString", "true");
        this.spark = new SparkSession.Builder().config(conf).getOrCreate();
    }

    protected <T> JavaRDD<T> getRDDFromList(final List<T> list) {
        return JavaSparkContext.fromSparkContext(spark.sparkContext()).parallelize(list);
    }
}
